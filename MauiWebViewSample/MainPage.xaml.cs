﻿using System.Reflection;

namespace MauiWebViewSample;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();

        using (var sr = new StreamReader(FileSystem.OpenAppPackageFileAsync("spa/index.html").GetAwaiter().GetResult()))
        {
            webView.Source = new HtmlWebViewSource()
            {
                Html = sr.ReadToEnd(),
                BaseUrl = "ms-appx-web:///spa/",
                //BaseUrl = "file:///android_asset/spa/",
            };
        }
    }
}

